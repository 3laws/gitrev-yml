Branch Management Tool
====================

Introduction
------------

A cli tool similar to gitflow tools but incorporating some needed features for our workflow It uses a manifest file and a config file to define the branch naming convention and the associated repos. We chose to do this rather than the gitflow cli tools method of modifying the git setup primarily because it lacked portability.

The command structure follows a “verb noun” structure with some optional flags

gitrev app start feature new_feature

verbs

* init
* list
* checkout / switch_to / co
* start
* delete (WIP)
* update
* finish
* push (WIP)
* pull
* merge
* make_tag (WIP)
* move_tag (WIP)
* delete_tag (WIP)
* reset

nouns

- feature
- bugfix
- develop
- hotfix
- release
- master
- additional arguments:
- remote: true/false
- dry_run: true/false
- commit: true/false
- manifest_file: full path
- filter: (WIP)

Details of verbs

init: will clone and pull the latest of the master branch and develop for all repos in the manifest file

list: will list all branches and tags that match for each repo.  Takes optional arguments of branch type, branch name, or filter

gitrev list release : will list all release branches
gitrev list release 16* or gitrev list release filter=16* : will list all release branches starting with 16
gitrev list filter=*-prod : will list all tags/branches ending with -prod

checkout (or switch_to, co): additional arguments of branch type and branch name: will checkout the specified branch from all repos

gitrev checkout feature some_feature : will checkout feature/some_feature
gitrev checkout release latest : will checkout the most recent release branch
gitrev switch_to bugfix some_bugfix: will checkout bugfix/some_bugfix

start: additional arguments of branch type and branch name and optional --branch argument: create a new branch

gitrev start feature some_feature : will create a new branch feature/some_feature branched from develop
gitrev start feature another_feature to_branch=feature/some_feature : will create a new branch feature/another feature from feature/some_feature

update: only use for release branches to pull changes and tag

finish: additional arguments of branch type and branch name and optional --branch argument: merge the branch

gitrev finish feature/another_feature to_branch=feature/some_feature : will merge feature/another_feature into feature/some_feature
gitrev finish feature/some_feature : will merge feature/some_feature into develop

push: used for pushing any local changes after running commands with the no_remote flag.

gitrev push release 16.11.04

pull: used to sync local repos after being offline or running commands locally with the no_remote flag

gitrev pull release 16.11.04

merge: used to merge one branch into another:

gitrev merge feature test_branch to_branch=develop
gitrev merge develop to_branch=feature/test_branch
gitrev merge bugfix test_branch to_branch=feature/test_branch

tag: used to add/move/delete tags on all repos in the manifest:

gitrev tag release/16.11.04 tag_name
gitrev --move tag release/16.11.04 tag_name
gitrev tag delete tag_name

reset: used to reset --hard all repos to a specific commit or pointer. If unspecified, default commit is HEAD

gitrev reset feature test_branch
gitrev reset feature test_branch commit=branch_name
gitrev reset develop commit=tag_name

Clone
------------
git clone git@gitlab.com:3laws/gitrev-yml.git


Installation
------------

add alias (replace $HOME/Repos with correct local path)
alias gitrev='$GPLAYBOOKS/../gitrev $@'
export GPLAYBOOKS="$HOME/Repos/gitrev/playbooks"
